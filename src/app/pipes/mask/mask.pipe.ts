import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mask'
})
export class MaskPipe implements PipeTransform {

  transform(value: any, arg): any {
    let newVal: any;
    if (arg === 'account') {
      newVal = value.substr(value.length-4, value.length)
    } 
    return newVal;
  }

}
