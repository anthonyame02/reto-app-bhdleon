import { NgModule } from '@angular/core';
import { MaskPipe } from './mask/mask.pipe';


@NgModule({
declarations: [MaskPipe],
imports: [],
exports: [MaskPipe],
})

export class PipesModule {}