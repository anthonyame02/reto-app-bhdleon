export interface error {
    statusCode : number;
    error : string;
    message : string;
}