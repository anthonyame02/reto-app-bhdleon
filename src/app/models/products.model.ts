export interface creditCard  {
    alias: string;
    number: string;
    availableAmountRD: number;
    availableAmountUS: number;
    isInternational: string;
    productType: string;
}

export interface  account  {
    alias: string;
    number: string;
    availableAmount: number;
    productType: string;
}

