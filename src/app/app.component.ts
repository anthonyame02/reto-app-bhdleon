import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserService } from './services/user/user.service';
import { } from '@ionic/storage';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  userData: any;
  userLogged: boolean = false;

  public secureMenu = [
    { title: 'Mis productos', page: '/dashboard', icon: 'icon_my_products_green' },
    { title: 'Transacciones', page: '/transactions', icon: 'icon_transactions_green' },
    { title: 'Ofertas', page: '/offers', icon: 'icon_offers_green' },
    { title: 'Configuración', page: '/configuration', icon: 'icon_config_green' },
  ];

  public menu = [
    { title: 'Contacto', page: '/contact', icon: 'icon_contact' },
    { title: 'Sucursales', page: '/branches', icon: 'icon_branches' }
  ];


  constructor(
    private platform: Platform,
    private users: UserService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private menuCtrl: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async loadUserData() {
    this.userData = await this.users.getUserInfo();

    if (this.userData.error) {
      this.userLogged = false;
    } else {
      this.userLogged = true;
    }
  }

  async closeSesion() {
    this.menuCtrl.close();
    this.storage.remove("id_token");
    this.router.navigate(['/login']);
  }

}
