import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from "@ionic/storage";

@Injectable()
export class HeaderHelper {


    constructor(private storage: Storage) { }

    async getToken()  {
        let token = await this.storage.get("id_token");
        return (token) ? token : false;
    }
}