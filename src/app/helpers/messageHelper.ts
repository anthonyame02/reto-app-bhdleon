import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable()
export class MessageHelper {

   loading: HTMLIonLoadingElement;

    constructor(
      private loadinCtrl: LoadingController,
      private toast: ToastController) { }

    async showMessageToast(message: string) {
      const toast = await this.toast.create({
        message: message,
        position: "top",
        cssClass: "toast-messaje-error",
        duration: 3000
      });
      toast.present();
    }
    
    async showLoading() {
      this.loading = await this.loadinCtrl.create({
        spinner: "bubbles",
        duration: 5000,
        translucent: false,
      });
  
      
      await this.loading.present(); 
    }
  
    async closeLoading() {
      this.loading.dismiss();
    }

}