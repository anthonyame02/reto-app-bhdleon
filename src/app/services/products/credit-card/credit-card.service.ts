import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { error } from '../../../models/error.model';
import { creditCard } from '../../../models/products.model';
import { HeaderHelper } from 'src/app/helpers/headerHelper';

@Injectable({
  providedIn: 'root'
})
export class CreditCardService {

  constructor(private http: HttpClient, private headerHelper: HeaderHelper) { }

  async getCreditCard() {

    let options = { headers: new HttpHeaders({ 'Authorization': `Bearer ${ await this.headerHelper.getToken()}` }) };

    return this.http.get(
      environment.API_URL + 'products/credit_cards',
      options
    ).pipe(
      map((creditCard: creditCard[]) => {
        return creditCard;
      }),
      tap(val => console.log(val)),
      catchError((err) => {
        return of(<error>err.error);
      }),
    ).toPromise()
  }

}
