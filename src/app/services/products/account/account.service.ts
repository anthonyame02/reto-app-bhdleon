import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { error } from '../../../models/error.model';
import { account } from '../../../models/products.model';
import { HeaderHelper } from 'src/app/helpers/headerHelper';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient, private headerHelper: HeaderHelper) { }

  async getAccounts() {

    let options = { headers: new HttpHeaders({ 'Authorization': `Bearer ${ await this.headerHelper.getToken()}` }) };

    return this.http.get(
      environment.API_URL + 'products/accounts',
      options
    ).pipe(
      map((account: account[]) => {
        return account;
      }),
      tap(val => console.log(val)),
      catchError((err) => {
        return of(<error>err.error);
      }),
    ).toPromise()
  }

}
