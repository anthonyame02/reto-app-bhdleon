import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { login } from '../../models/login.model';
import { environment } from '../../../environments/environment';
import { of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { token } from '../../models/token.model';
import { error } from '../../models/error.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient, private storage: Storage) {
  }
  /** Guardamos el token en el storage */
  private async setSession(authResult: token) {
    // await this.storage.set('id_token', authResult.access_token); 
    localStorage.setItem("id_token", authResult.access_token);
    this.storage.set("id_token", authResult.access_token)
  }

  /** metodo de autenticacion del */
  login(body: login) {
    return this.http.post(
      environment.API_URL + 'sign_in', body
    ).pipe(
      map(async (data: token) => {
        await this.setSession(data);
        return true;
      }),
      tap(val => console.log(val)),
      catchError((err) => {
        console.log('Error', <error>err.error)
        return of(<error>err.error);
      }),
    ).toPromise()
  }



}

