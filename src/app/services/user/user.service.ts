import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { of , throwError} from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { error } from '../../models/error.model';
import { user } from '../../models/user.model';
import { HeaderHelper } from 'src/app/helpers/headerHelper';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private headerHelper: HeaderHelper) {}

  async getUserInfo() {

    let options = { headers: new HttpHeaders({ 'Authorization': `Bearer ${ await this.headerHelper.getToken()}` }) };
    
    return this.http.get(
      environment.API_URL + 'user_data',
      options
    ).pipe(
      map((user: user) => {
        return user;
      }),
      tap(val => console.log(val)),
      catchError((err) => {
        console.log('Error', <error>err.error)
        return of(<error>err.error);
      }),
    ).toPromise()
  }

}
