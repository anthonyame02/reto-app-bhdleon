import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  headerTitle:string = "Configuración";
  menu: string = "icon_config_gray";
  
  constructor() { }

  ngOnInit() {
  }

}
