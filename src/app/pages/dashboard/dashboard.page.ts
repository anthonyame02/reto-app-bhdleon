import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/products/account/account.service';
import { CreditCardService } from 'src/app/services/products/credit-card/credit-card.service';
import { creditCard, account } from 'src/app/models/products.model';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {

  headerTitle: string = "Mis productos";
  creditCards: any;
  accounts: any;
  menu: string = "icon_account_gray";

  constructor(
    private credit: CreditCardService,
    private account: AccountService,
    private router: Router) { }

  async ionViewDidEnter() {
    this.creditCards = await this.credit.getCreditCard();
    this.accounts = await this.account.getAccounts();
  }

  openProductDetail(product: any){
    let navigationExtras: NavigationExtras = { state: { product: product } };
    this.router.navigate(['/product-detail'], navigationExtras);
  }



}
