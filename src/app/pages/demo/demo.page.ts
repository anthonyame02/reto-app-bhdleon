import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.page.html',
  styleUrls: ['./demo.page.scss'],
})
export class DemoPage implements OnInit {


  demoImages: any = [
    { image: 'slide_1.svg', text: "Puedes ver tus productos" },
    { image: 'slide_2.svg', text: "Ahorra tiempo realizando transacciones de forma rápida y segura"},
    { image: 'slide_3.svg', text: "Aprovecha las ofertas que tenemos para ti" }
  ]

  constructor() { }

  ngOnInit() {
  }

}
