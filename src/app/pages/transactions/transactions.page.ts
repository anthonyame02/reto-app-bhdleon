import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {

  headerTitle:string = "Transacciones";
  menu: string = "icon_transactions_gray";
  
  constructor() { }

  ngOnInit() {
  }

}
