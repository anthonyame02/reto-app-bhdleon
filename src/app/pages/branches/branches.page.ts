import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.page.html',
  styleUrls: ['./branches.page.scss'],
})
export class BranchesPage implements OnInit {
  
  headerTitle:string = "Sucursales";
  
  constructor() { }

  ngOnInit() {
  }

}
