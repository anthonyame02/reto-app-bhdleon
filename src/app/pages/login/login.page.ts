import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { login } from 'src/app/models/login.model';
import { Router } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { MessageHelper } from 'src/app/helpers/messageHelper';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: string = "";
  pass: string = "";

  constructor(
    private auth: AuthenticationService, 
    private router: Router,
    private message: MessageHelper) { }

  ngOnInit() {
  }

  async login() {

    this.message.showLoading()
    
    let result: any = await this.auth.login(new login(this.user, this.pass));
    
    this.message.closeLoading()
    
    if(!this.user.trim()) { 
      return this.message.showMessageToast("Por favor introduzca su documento de identidad para poder iniciar sesión");
    }

    if(!this.pass.trim()) { 
      return this.message.showMessageToast("Por favor introduzca su contraseña para poder iniciar sesión");
    }
    
    if(result.statusCode === 404) { 
      return this.message.showMessageToast(result.message);
    }

    return this.router.navigate(['/dashboard']);
    
  }

  


}
