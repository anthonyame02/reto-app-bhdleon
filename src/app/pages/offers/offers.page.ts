import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {

  headerTitle:string = "Ofertas";
  menu: string = "icon_offers_gray";
  
  constructor() { }

  ngOnInit() {
  }

}
