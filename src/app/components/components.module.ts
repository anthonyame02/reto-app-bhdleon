import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { MenuFooterComponent } from './menu-footer/menu-footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuFooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    IonicModule.forRoot()
  ],
  providers: [],
  entryComponents: [
    HeaderComponent,
    MenuFooterComponent
  ],
  exports: [
    HeaderComponent,
    MenuFooterComponent
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }


