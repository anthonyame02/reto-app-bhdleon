import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() headerTitle: string;
  @Input() backButtonActive: boolean;
  
  constructor(private storage: Storage, private router: Router) { }

  async ngOnInit() {

    let token = await this.storage.get("id_token");
      
      if (!token) {
        this.router.navigate(['/login']);
      }
  }

}
