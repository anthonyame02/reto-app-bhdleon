import { Component, OnInit, Input } from '@angular/core';
import { LoginPage } from 'src/app/pages/login/login.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-footer',
  templateUrl: './menu-footer.component.html',
  styleUrls: ['./menu-footer.component.scss'],
})
export class MenuFooterComponent implements OnInit {

  menuOptions = [
    { icon: "icon_account_gray", iconActive: "icon_my_products_green", text: "Mis productos", page: "dashboard" },
    { icon: "icon_transactions_gray", iconActive: "icon_transactions_green", text: "Transacciones", page: "transactions" },
    { icon: "icon_offers_gray", iconActive: "icon_offers_green", text: "Ofertas", page: "offers" },
    { icon: "icon_config_gray", iconActive: "icon_config_green", text: "Configuración", page: "configuration" }
  ];
  @Input() menu: string;

  constructor(private router: Router) { }

  ngOnInit() { }

  async clickOption(option: any) {
    this.router.navigate([`/${option.page}`]);
  }

}
